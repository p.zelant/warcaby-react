import React from 'react';
import BoardScreen from "./Screens/BoardScreen";
import StartScreen from "./Screens/StartScreen";
import GameOverScreen from "./Screens/GameOverScreen";

export default class Game extends React.Component{

    STATE_PLAYING = 'playing';
    STATE_START_SCREEN = 'start_screen';
    STATE_GAMEOVER = 'gameover';

    gameOverStats = {
         winner:null, winnerName:null, beatenWhite:0, beatenBlack:0
    };

    constructor(props) {
        super(props);
        this.state = {gameState: this.STATE_START_SCREEN};
    }

    gameStart(){
        this.setState({gameState: this.STATE_PLAYING});
    }

    gameOver(gameOverStats){
        console.log(gameOverStats);
        this.gameOverStats = gameOverStats;
        this.setState({gameState: this.STATE_GAMEOVER});
    }

    render() {
        if(this.state.gameState === this.STATE_START_SCREEN) {
            return (<StartScreen gameStart={() => this.gameStart()}/>);
        } else if(this.state.gameState === this.STATE_PLAYING) {
            return (<BoardScreen gameOver={this.gameOver.bind(this)}/>);
        } else if(this.state.gameState === this.STATE_GAMEOVER) {
            return (<GameOverScreen gameStart={() => this.gameStart()} winner={this.gameOverStats.winner} winnerName={this.gameOverStats.winnerName} beatenWhite={this.gameOverStats.beatenWhite} beatenBlack={this.gameOverStats.beatenBlack} />);
        } else {
            return (<div>Wystąpił błąd w stanie gry. Aktualny stan: {this.gameState}</div>);
        }
    }

}