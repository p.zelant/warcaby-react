import React from 'react';

export default class StartScreen extends React.Component{
    constructor(props) {
        super(props);
        this.props = props;
    }

    render() {
        return (<div className="startScreen">
            <h1>Warcaby v.0.1</h1>
            <button onClick={this.props.gameStart} value = "Zacznij grę">Zacznij grę</button>
        </div> )
    }
}