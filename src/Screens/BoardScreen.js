import React from 'react';
import Moves from "../Components/Moves";
import Tile from "../Components/Tile";
import GameStats from "../Components/GameStats";

import CoordsHelper from "../Components/CoordsHelper";

export default class BoardScreen extends React.Component {

    BOARD_SIZE = 10;
    CHECKER_BLACK = 'black';
    CHECKER_WHITE = 'white';
    tiles = []; // kafelki
    Moves; // nasza klasa do ruchu
    CoordsHelper;
    initialState = {selected: {x: 0, y: 0}, moves: [], beatMoves: []}; // stan początkowy
    whosTurn; // aktualna tura
    gameOver; // funkcja dla konca gry

    playerWhiteName = null; // imię gracza białego
    playerBlackName = null; // imię gracza czarnego
    beatenCountWhite = 0;
    beatenCountBlack = 0;
    PDF_PRINT_SERVICE_URL = 'http://localhost/warcaby/public/warcaby/pdf/';

    constructor(props) {
        super(props);

        this.gameOver = props.gameOver;
        while (this.playerWhiteName === null) {
            this.playerWhiteName = prompt('Gracz BIAŁY: podaj imię.');
        }

        while (this.playerBlackName === null) {
            this.playerBlackName = prompt('Gracz CZARNY: podaj imię.');
        }

        this.whosTurn = this.CHECKER_WHITE;
        this.state = this.initialState;

        // deklarowanie planszy w pamięci - wszystkie pola puste
        for (let x = 1; x <= this.BOARD_SIZE; x++) {
            this.tiles[x] = [];
            for (let y = 1; y <= this.BOARD_SIZE; y++) {
                this.tiles[x][y] = null;
            }
        }

        this.CoordsHelper = new CoordsHelper([]);
        this.Moves = new Moves([]);

        this.initCheckers();

    }

    /**
     * Dodajemy pionki startowe do planszy
     */
    initCheckers() {
        this.tiles[1][1] = {type: this.CHECKER_BLACK};
        this.tiles[1][3] = {type: this.CHECKER_BLACK};
        this.tiles[1][5] = {type: this.CHECKER_BLACK};
        this.tiles[1][7] = {type: this.CHECKER_BLACK};
        this.tiles[1][9] = {type: this.CHECKER_BLACK};
        this.tiles[2][2] = {type: this.CHECKER_BLACK};
        this.tiles[2][4] = {type: this.CHECKER_BLACK};
        this.tiles[2][6] = {type: this.CHECKER_BLACK};
        this.tiles[2][8] = {type: this.CHECKER_BLACK};
        this.tiles[2][10] = {type: this.CHECKER_BLACK};
        this.tiles[3][1] = {type: this.CHECKER_BLACK};
        this.tiles[3][3] = {type: this.CHECKER_BLACK};
        this.tiles[3][5] = {type: this.CHECKER_BLACK};
        this.tiles[3][7] = {type: this.CHECKER_BLACK};
        this.tiles[3][9] = {type: this.CHECKER_BLACK};
        this.tiles[4][2] = {type: this.CHECKER_BLACK};
        this.tiles[4][4] = {type: this.CHECKER_BLACK};
        this.tiles[4][6] = {type: this.CHECKER_BLACK};
        this.tiles[4][8] = {type: this.CHECKER_BLACK};
        this.tiles[4][10] = {type: this.CHECKER_BLACK};

        this.tiles[7][1] = {type: this.CHECKER_WHITE};
        this.tiles[7][3] = {type: this.CHECKER_WHITE};
        this.tiles[7][5] = {type: this.CHECKER_WHITE};
        this.tiles[7][7] = {type: this.CHECKER_WHITE};
        this.tiles[7][9] = {type: this.CHECKER_WHITE};
        this.tiles[8][2] = {type: this.CHECKER_WHITE};
        this.tiles[8][4] = {type: this.CHECKER_WHITE};
        this.tiles[8][6] = {type: this.CHECKER_WHITE};
        this.tiles[8][8] = {type: this.CHECKER_WHITE};
        this.tiles[8][10] = {type: this.CHECKER_WHITE};
        this.tiles[9][1] = {type: this.CHECKER_WHITE};
        this.tiles[9][3] = {type: this.CHECKER_WHITE};
        this.tiles[9][5] = {type: this.CHECKER_WHITE};
        this.tiles[9][7] = {type: this.CHECKER_WHITE};
        this.tiles[9][9] = {type: this.CHECKER_WHITE};
        this.tiles[10][2] = {type: this.CHECKER_WHITE};
        this.tiles[10][4] = {type: this.CHECKER_WHITE};
        this.tiles[10][6] = {type: this.CHECKER_WHITE};
        this.tiles[10][8] = {type: this.CHECKER_WHITE};
        this.tiles[10][10] = {type: this.CHECKER_WHITE};
    }

    render() {

        let tilesDraw = this.tiles.map((cols, x) => {
            return cols.map((tile, y) => {
                return (
                    <Tile x={x} y={y} tile={tile} selected={this.state.selected} moves={this.state.moves}
                          beatMoves={this.state.beatMoves} onClick={() => this.selectTile(x, y, tile)}/>
                );
            });
        });

        return (
            <div>
                <h1>Warcaby</h1>

                <div className="boardLettersTop"><span>a</span><span>b</span><span>c</span><span>d</span><span>e</span><span>f</span><span>g</span><span>h</span><span>i</span><span>j</span></div>

                <div className="boardWrapper">
                    <div className="boardNumbersLeft"><span>1</span><span>2</span><span>3</span><span>4</span><span>5</span><span>6</span><span>7</span><span>8</span><span>9</span><span>10</span></div>
                     <div id="board" key="board1" className="board">
                        {tilesDraw}
                    </div>
                </div>

            <GameStats turn={this.whosTurn}
                       turnName={this.whosTurn === this.CHECKER_WHITE ? 'GRACZ BIAŁY' : 'GRACZ CZARNY'}
                       playerBlackName={this.playerBlackName}
                       playerWhiteName={this.playerWhiteName}
                       beatenCountBlack={this.beatenCountBlack}
                       beatenCountWhite={this.beatenCountWhite}/>
                <a target="_blank" href={this.parseBoardToHref()}>(drukuj planszę)</a>
            </div>);
    }

    /**
     * Kafelek został zaznaczony - kliknięty
     * @param x
     * @param y
     * @param cell
     */
    selectTile = (x, y, cell) => {
        let selectedChecker = this.tiles[x][y]; // zazaczony kafelek
        if (selectedChecker !== null) { // zaznaczono pionek
            if (selectedChecker.type === this.whosTurn) {
                this.setState({
                    selected: {x: x, y: y}, // zaznaczamy koordynaty na planszy gdzie kliknęliśmy
                    moves: this.Moves.getMoves(x, y, this.tiles), // sprawdzamy mozliwe ruchy
                    beatMoves: this.Moves.getBeatMoves(x, y, this.tiles) // sprawdzamy gdzie mamy bicie
                });
            }

        } else if (this.state.moves.indexOf(this.CoordsHelper.parse(x, y)) !== -1) { // kliknięte koordynaty były w tablicy możliwych ruchów
            this.moveChecker(x, y); // poruszamy pionkiem
            this.switchTurn();
        } else if (this.state.beatMoves.hasOwnProperty(this.CoordsHelper.parse(x, y))) { // kliknięto koordynaty bicia
            let beatCoords = this.state.beatMoves[this.CoordsHelper.parse(x, y)]; // koordynaty bicia
            this.beatChecker(x, y, beatCoords[0], beatCoords[1]); // bijemy pionek
        } else {
            this.setState(this.initialState); // kliknięto puste pole, wyczyść stan
        }
    };

    /**
     * Poruszamy pionkiem we wskazane koordynaty
     * @param x
     * @param y
     */
    moveChecker(x, y) {
        this.tiles[x][y] = this.tiles[this.state.selected.x][this.state.selected.y]; // zmieniamy miejscami aktualnie zaznaczony z miejscem kliknięcia
        this.tiles[this.state.selected.x][this.state.selected.y] = null; // czyścimy poprzednie miejsce
        this.setAsKing(x, y, this.tiles[x][y]); // ustawiamy damkę
        // TODO: sprawdzać czy mamy jeszcze bicia i zmieniać turę jeśli nie mamy bicia

        this.setState(this.initialState); // resetujemy stan - odznaczamy zaznaczenie, czyścimy możliwe ruchy
    }

    /**
     * Zbijamy pionka w zadanych koordynatach
     * @param x
     * @param y
     * @param beatenX koordynaty pionka do zbicia
     * @param beatenY
     */
    beatChecker(x, y, beatenX, beatenY) {
        let beatenChecker = this.tiles[beatenX][beatenY];
        if (beatenChecker.type === this.CHECKER_WHITE) {
            this.beatenCountWhite++;
        } else {
            this.beatenCountBlack++;
        }
        this.tiles[beatenX][beatenY] = null; // kasujemy zbity pionek
        this.moveChecker(x, y); // poruszamy pionkiem - stan się resetuje
        let fromAnotherBeat = true;
        let beatMoves = this.Moves.getBeatMoves(x, y, this.tiles, fromAnotherBeat); // sprawdzamy czy mamy jeszcze bicia
        if (Object.keys(beatMoves).length !== 0) { // mamy bicia
            this.setState({beatMoves: beatMoves, moves: [], selected: {x: x, y: y}});
        } else {
            this.switchTurn();
        }
    }

    /**
     * Ustawiamy pionek jako damkę
     * @param x
     * @param y
     * @param selectedChecker
     */
    setAsKing(x, y, selectedChecker) {
        let setAsKing = false;
        if (selectedChecker.type === this.CHECKER_WHITE && x === 1) {
            setAsKing = true;
        } else if (selectedChecker.type === this.CHECKER_BLACK && x === this.BOARD_SIZE) {
            setAsKing = true;
        }

        if (setAsKing === true) {
            this.tiles[x][y].king = true;
        }
    }

    switchTurn() {
        this.whosTurn = this.whosTurn === this.CHECKER_BLACK ? this.CHECKER_WHITE : this.CHECKER_BLACK;
        let counter = this.countCheckersAndMoves(this.whosTurn);

        if (counter.count === 0 || (counter.moves === 0 && counter.beats === 0)) { // sprawdzamy czy dla aktualnego gracza mamy dostępne ruchy, bicia i ile mamy pionków na planszy
            this.triggerGameOver();
        }
    }

    /**
     * Policz wszystkie bicia, ruchy i ilość pionków na planszy
     * @param checkerType
     * @returns {{moves: number, count: number, beats: number}}
     */
    countCheckersAndMoves(checkerType) {

        let counter = {moves: 0, count: 0, beats: 0};

        for (let x = 1; x <= this.BOARD_SIZE; x++) {
            for (let y = 1; y <= this.BOARD_SIZE; y++) {
                let checker = this.tiles[x][y];
                if (checker !== null && checker.type === checkerType) {
                    counter.moves += this.Moves.getMoves(x, y, this.tiles).length;
                    counter.beats += Object.keys(this.Moves.getBeatMoves(x, y, this.tiles)).length;
                    counter.count++;
                }
            }
        }

        return counter;
    }

    triggerGameOver() {
        let winnerName = this.whosTurn === this.CHECKER_BLACK ? this.playerWhiteName : this.playerBlackName;
        let winner = this.whosTurn === this.CHECKER_BLACK ? 'Gracz biały' : 'Gracz czarny';

        let gameOverStats = {
            winner: winner,
            winnerName: winnerName,
            beatenWhite: this.beatenCountWhite,
            beatenBlack: this.beatenCountBlack
        };

        this.gameOver.bind(this, gameOverStats);
        this.gameOver(gameOverStats); // KONIEC GRY
    }

    parseBoardToHref(){

        let hrefJson = {positions:[], playerWhiteName: this.playerWhiteName, playerBlackName: this.playerBlackName, beatenCountBlack: this.beatenCountBlack, beatenCountWhite: this.beatenCountWhite, actualTurn: this.whosTurn === this.CHECKER_WHITE?'GRACZ BIALY':'GRACZ CZARNY'};
        for(let x=1;x<=this.BOARD_SIZE;x++) {
            for(let y=1;y<=this.BOARD_SIZE; y++) {
                if(this.tiles[x][y] !== null) {
                    hrefJson.positions.push([x,y,this.tiles[x][y].type, this.tiles[x][y].king === true]);
                }
            }
        }
        return this.PDF_PRINT_SERVICE_URL + JSON.stringify(hrefJson);
    }
}