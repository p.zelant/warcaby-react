import React from 'react';

export default class GameOverScreen extends React.Component {

    constructor(props){
        super(props);
        this.props = props;
    }

    render() {
        return (
            <div><h1>Koniec gry</h1>
                <h2>Wygrywa gracz: {this.props.winner} ({this.props.winnerName})</h2>
                <div>
                    Ilość zbitych białych: {this.props.beatenWhite}<br/>
                    Ilość zbitych czarnych: {this.props.beatenBlack}
                </div>
                <button onClick={this.props.gameStart} value = "Zacznij grę">Zacznij grę od nowa</button>
            </div>
        )
    }
}