import React from 'react';
import CoordsHelper from "./CoordsHelper";

export default class Tile extends React.Component {
    tile;
    onClick;

    constructor(props) {
        super(props);
        this.props = props;
        this.CoordsHelper = new CoordsHelper([]);
    }

    render() {

        let x = this.props.x;
        let y = this.props.y;
        let selectedX = this.props.selected.x;
        let selectedY = this.props.selected.y;
        let tile = this.props.tile;
        let beatMoves = this.props.beatMoves;
        let moves = this.props.moves;
        let onClick = this.props.onClick;

        let className = "tile";
        if (selectedX === x && selectedY === y) {// czy kafelek jest zaznaczony
            className += " selected";
        }
        if ((y + x) % 2 === 0) { // rysujemy szare kawałki planszy
            className += " grey";
        }
        if (moves.indexOf(this.CoordsHelper.parse(x, y)) !== -1) { // rysujemy możliwy ruch
            className += " canMove";
        }
        if (beatMoves.hasOwnProperty(this.CoordsHelper.parse(x, y))) { // rysujemy możliwe bicia
            className += " canMove";
        }
        let checker = null;
        if (tile !== null) { // rysujemy pionek
            let tileClassName = tile.type + "_checker checker";
            if(tile.king === true) {
                tileClassName += " king";
            }
            checker = (<span key={x} className={tileClassName}>&nbsp;</span>);
        }
        return (<div onClick={onClick} id={y + "_" + x} className={className}>{checker}</div>);

    }
}