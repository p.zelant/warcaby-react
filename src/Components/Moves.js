import React from 'react';
import CoordsHelper from "./CoordsHelper";

export default class Moves extends React.Component {

    constructor(){
        super([]);
        this.CoordsHelper = new CoordsHelper();
    }

    /**
     * Kierunki ruchu pionków
     * @type {{white: number[][], black: number[][]}}
     */
    moveVectors = {
        'black': [[1, 1], [1, -1]],
        'white': [[-1, 1], [-1, -1]]
    };

    allMoveVectors = [
        [1, 1], [1, -1], [-1, 1], [-1, -1]
    ];

    /**
     * Wyznaczenie wszystkich możliwych ruchów
     * @param x
     * @param y
     * @param tiles
     * @returns {[]}
     */
    getMoves(x, y, tiles) {
        let selectedChecker = tiles[x][y];
        let moves = [];

        if(selectedChecker.king === true) { // mamy damkę trzeba obliczyć ruch osobno
            /* bug, bo zgłasza że key is never used mimo, że go używam i działa @see https://github.com/eslint/eslint/issues/12117 */
            for(let key in this.allMoveVectors) {
                let vector = this.allMoveVectors[key];
                let kingVector = vector;
                while (this.canMove(x,y,kingVector, tiles)) { // lecimy po wektorze damki aż dojdziemy do zajętego pola, lub końca planszy
                    moves.push(this.CoordsHelper.parseVector(x, y, kingVector)); // dodajemy koordynaty damki
                    kingVector = [kingVector[0]+(vector[0]), kingVector[1]+(vector[1])]; // przedłużamy wektor
                }
            }
        } else {
            for (let key in this.moveVectors[selectedChecker.type]) { // pobieramy wektory ruchu dla typu pionka
                let vector = this.moveVectors[selectedChecker.type][key];

                if (this.canMove(x, y, vector, tiles)) { // czy może się tam poruszyć
                    moves.push(this.CoordsHelper.parseVector(x, y, vector)); // dodajemy koordynaty
                }
            }

        }

        return moves;
    }

    /**
     * Wyznaczamy bicia
     * @param x
     * @param y
     * @param tiles
     * @param fromAnotherBeat poprzednio też było bicie
     */
    getBeatMoves(x, y, tiles, fromAnotherBeat) {
        let selectedChecker = tiles[x][y];

        let beatMoves = {};

        if(selectedChecker.king === true && fromAnotherBeat !== true) {
            /* bug, bo zgłasza że key is never used mimo, że go używam i działa @see https://github.com/eslint/eslint/issues/12117 */
            for(let key in this.allMoveVectors) {
                let vector = this.allMoveVectors[key];
                let kingVector = vector;
                while (this.canMove(x,y,kingVector, tiles)) { // lecimy po wektorze damki aż dojdziemy do zajętego pola, lub końca planszy
                    kingVector = [kingVector[0]+(vector[0]), kingVector[1]+(vector[1])]; // przedłużamy wektor
                }
                // natrafiliśmy na przeszkode, trzeba zbadać czy jest bicie
                let typeInVectorTile = this.getCheckerType(x, y, kingVector, tiles); // pobieramy typ pionka w wektorze ruchu
                let beatAndMoveVector = [kingVector[0]+vector[0], kingVector[1]+vector[1]];
                if(this.checkIfCanBeatAndMove(x, y, beatAndMoveVector, tiles, typeInVectorTile, selectedChecker, beatMoves, vector)){
                    beatMoves[(this.CoordsHelper.parseVector(x, y, beatAndMoveVector))] = [x + kingVector[0], y + kingVector[1]]; // dodajemy koordynaty ruchu zaznaczając też jaki pionek będzie zbity
                }
            }
        } else {
            /* bug @see https://github.com/eslint/eslint/issues/12117 */
            for (let key in this.allMoveVectors) { // bicia są we wszystkich kierunkach
                let vector = this.allMoveVectors[key];
                let doubleVector = [vector[0] * 2, vector[1] * 2]; // podwójny wektor do ruchu za pionek do sprawdzenia czy za pionkiem mamy puste pole
                let typeInVectorTile = this.getCheckerType(x, y, vector, tiles); // pobieramy typ pionka w wektorze ruchu

                if(this.checkIfCanBeatAndMove(x, y, doubleVector, tiles, typeInVectorTile, selectedChecker, beatMoves, vector)){
                    beatMoves[(this.CoordsHelper.parseVector(x, y, doubleVector))] = [x + vector[0], y + vector[1]]; // dodajemy koordynaty ruchu zaznaczając też jaki pionek będzie zbity
                }
            }
        }

        return beatMoves;
    }

    checkIfCanBeatAndMove(x, y, beatAndMoveVector, tiles, typeInVectorTile, selectedChecker, beatMoves, vector) {
        let canMove = this.canMove(x, y, beatAndMoveVector, tiles); // sprawdzamy czy za wykrytym pionkiem będzie miejsce do bicia
        if (typeInVectorTile !== null && typeInVectorTile !== selectedChecker.type && canMove) { // sprawdzamy czy typ pionka w stronę wektora jest różny od zaznaczonego
           return true;
        }
        return false;
    }

    /**
     * Sprawdzamy typ pionka w podanych koordynatach przesuniętych o wektor
     * @param x
     * @param y
     * @param vector
     * @param tiles
     * @returns {null|*}
     */
    getCheckerType(x, y, vector, tiles) {
        if (tiles[x + vector[0]] === undefined || tiles[x + vector[0]][y + vector[1]] === undefined || tiles[x + vector[0]][y + vector[1]] === null) {
            return null;
        }

        return tiles[x + vector[0]][y + vector[1]].type;
    }

    /**
     * Czy możemy się poruszyć na koordynaty przesunięte o wektor
     * @param x
     * @param y
     * @param vector
     * @param tiles
     * @returns {boolean}
     */
    canMove(x, y, vector, tiles) {
        if (tiles[x + vector[0]] === undefined) {
            return false;
        }
        return tiles[x + vector[0]][y + vector[1]] === null;
    }


}