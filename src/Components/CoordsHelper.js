import React from 'react';

export default class CoordsHelper extends React.Component {
    parse(x, y) {
        return (x) + "-" + (y);
    }

    /**
     * Funkcja tworzy stringa z koordynatami dla ruchu
     * @param x
     * @param y
     * @param vector
     * @returns {string}
     */
    parseVector(x, y, vector) {
        return (x + vector[0]) + "-" + (y + vector[1]);
    }
}