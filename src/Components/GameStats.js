import React from 'react';

export default class GameStats extends React.Component {
    constructor(props) {
        super(props);
        this.props = props;
    }

    render() {
        return (<div className="gameStats">
            Aktualna tura: <strong>{this.props.turnName}</strong><br/>
            <br/>
            Gracz biały <strong>{this.props.playerWhiteName}</strong> (zbito czarnych: {this.props.beatenCountBlack})<br/>
            Gracz czarny <strong>{this.props.playerBlackName}</strong> (zbito białych: {this.props.beatenCountWhite})<br/>

        </div>);
    }
}